# enex2bookmarks

Convert evernote .enex file to a bookmarks html file.
Quick and dirty.

## Reqirements

* bash
* java

## Howto

Rename your input file: Bookmarks.enex
Will write output to: evermarks.html

```./convert.sh```
